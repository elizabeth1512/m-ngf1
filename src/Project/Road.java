package Project;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.Timer; 


public class Road extends JPanel implements ActionListener, Runnable {
	//чтобы ехала бдует запускать функцию каждые 20 милисеунд наш такймер будет 
	//выполнять функцию экшн перформед 
	Timer mainTimer = new Timer(20, this);
	
	//вызываем картинку и потом гетимаге возвращает
	Image img = new ImageIcon("res/dorozhechka1.png").getImage();

	//что есть на дороге
	Player p = new Player();
	
	//для того чтобы враги создавались постоянно и не через один
	//и тот промежуток времени
	
	Thread enemiesFactory = new Thread(this);
	
	List<Enemy> enemies = new ArrayList<Enemy>();
	
	//запустить таймер для этого надо дслеать конструктор и там запустить
	public Road() {
		mainTimer.start();
		enemiesFactory.start();
		//класс который должен реализовывать класс киилиснер только нажатие 
		//отпускание и существует адаптер только те которые нам нужны 
		addKeyListener(new myKeyAdapter());
		
		//сфокусировать чтобы все клавиши обрабатывались
		setFocusable(true);
	}
	
	private class myKeyAdapter extends KeyAdapter {
		
		//при нажатии клавиши мы гвоорим чтобы мащина на это среагировала
		public void keyPressed(KeyEvent e) {
			p.keyPressed(e);
		}
		public void keyReleased(KeyEvent e) {
			p.keReleased(e);
		}
	}
	
	//метод вызывается кода нужно перерисовать, автоматически вызывается ну модно самим тоже
	public void paint(Graphics g) {
		//введение типов, умеет рисовать только этот тип
		g = (Graphics2D) g;
		//теперь  можно рисовать сперва что рисовать потом где рисовать и нул это 
		//рисуем слои
		g.drawImage(img, p.layer1, 0, null);
		g.drawImage(img, p.layer2, 0, null);
		//координаты машины
		g.drawImage(p.img, p.x, p.y, null);
		
		double v = (200/Player.MAX_V) * p.v;//макс скорость делим на максимальную скорость
		//нашу и умнжаем на количество пикселей
		g.setColor(Color.WHITE);
		Font font = new Font("Arial",Font.ITALIC, 20);
		g.setFont(font);
		g.drawString("Speed: " +  v +"km/h", 100, 30);
		
		
		Iterator<Enemy> i = enemies.iterator();//чтобы пробежаться по коллекции
		while(i.hasNext()) { //пока существует какой то элемент
			Enemy e = i.next();//получаем каждый раз новый обьект
			if (e.x >= 2400 || e.x <= -2400) {
				i.remove();
			}else{
				e.move();//если никуда не уехал вызываем этот метод
			g.drawImage(e.img, e.x, e.y, null);//рисуем его изображение
			
		}
		}
	
	}
	
	public void actionPerformed(ActionEvent e) {
		
		p.move(); //ехать 
		repaint(); //вызывает пэйнт, перерисовывать
		testCollisionWithEnemies();//проверить столковновение с врагами
		testWin();
	}
	private void testWin() {
		
		if (p.s > 20000) {
			JOptionPane.showMessageDialog(null, "YEEE! You win!");
			System.exit(0);
		}
		
	}

	private void	testCollisionWithEnemies() {//в этом методе все проверяем
		Iterator<Enemy> i = enemies.iterator();
		while  (i.hasNext()) {
			Enemy e = i.next(); //по коллекции получаем доступ
			if (p.getREct().intersects(e.getREct())) { //пересекаются друг с другом
			   JOptionPane.showMessageDialog(null, "You lose!");
			   System.exit(1); 
			
			}
		}
	
}

	@Override
	public void run() {
		
		while(true) {
			// для получение случайных чисел
			Random rand = new Random();
			try {
				Thread.sleep(rand.nextInt(2000));
				
				enemies.add(new Enemy(1200, rand.nextInt(480), rand.nextInt(50),this));
				
			} catch (InterruptedException e) {
				e.printStackTrace();
				
			}
			
		}
		
	}

}

